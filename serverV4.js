"use strict";

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var user_file=require('./user.json');



const global = require('./global');
const cors= require('cors');
var port=process.env.PORT || 3000;
app.use(bodyParser.json());
var users = require('./controllers/UserController');
var userlog = require('./controllers/UserLogcontrol');
app.use(bodyParser.json());
const URL_BASE= '/techu-peru/v2/'
var requestJSON = require('request-json');
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu42db/collections/';
const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

app.use(bodyParser.json());

app.use(cors());
app.options('*', cors());

// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users/:id',
 function(req, res) {
   var id = req.params.id;
   var queryStringID = 'q={"ID":' + id + '}&';
   console.log(queryStringID);
   console.log("GET /techu-peru/v2");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('users?' + queryString + queryStringID + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users/:id/accounts',
 function(req, res) {
   var id = parseInt(req.params.id);
      var queryStringID = 'q={"id_user":' + id + '}&';
      console.log(queryStringID);
   console.log("GET /techu-peru/v2");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   var url = 'accounts?'+ queryString + queryStringID + apikeyMLab;
   console.log(url);
   httpClient.get(url,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'CONTRATO'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});

app.post(URL_BASE + 'users',
 function(req, res) {
   var clienteMlab =requestJSON.createClient(baseMLabURL);
   console.log(req.body);
   clienteMlab.get("users?"+apikeyMLab,
    function(error, respuestaMLab, body) {

        if(error) {
            response = {"msg" : "Error "}
            res.status(500);
            res.send(response);
        }else{

            var  newID=body.length +1;
            console.log("newID:" + newID);
      //            console.log("Iinresa nuevi");
            var newUser={
              "ID":newID,
              "first_name":req.body.first_name,
              "last_name": req.body.last_name,
              "email":req.body.email,
              "password":req.body.password
              };
              clienteMlab.post(baseMLabURL+"users?"+apikeyMLab,newUser,
                function(error, respuestaMLab, body){
                  console.log("body:"+body);
                  res.status(201);
                  res.send(body);
              });
        }
    });

//      console.log("termino de asignar nuevoID");
        //podemos obviar el pasarle la  baseMLabURL

});

//PUT users con parámetro 'id'
app.put(URL_BASE + 'users/:id',
function(req, res) {
 var id = req.params.id;
 var queryStringID = 'q={"ID":' + id + '}&';
 var clienteMlab = requestJSON.createClient(baseMLabURL);
 clienteMlab.get('users?'+ queryStringID + apikeyMLab,
   function(error, respuestaMLab, body) {
     if(error) {
       response = {"msg" : "Error "}
       res.status(500);
       res.send(response); }
       else {
         console.log("ingreso a modificar");
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
    console.log(req.body);
    console.log(cambio);
    console.log("en proceso");
    clienteMlab.put(baseMLabURL +'users?' + queryStringID + apikeyMLab, JSON.parse(cambio),
     function(error, respuestaMLab, body) {
       console.log("evalua  el valor  ****** N");
       console.log("body valor N:"+ body); // body.n devuelve 1 si pudo hacer el update
      //res.status(200).send(body);
     console.log("termina de evaluar valor  ****** N");
      res.send(body);
     });
   }
   });
});


// Petición PUT con id de mLab (_id.$oid)   ----- ME SALE ELIMINADO EN LA BASE DE DATOS
 app.put(URL_BASE + 'usersmLab/:id',
   function (req, res) {
     var id = req.params.id;
     let userBody = req.body;
     var queryString = 'q={"ID":' + id + '}&';
     var httpClient = requestJSON.createClient(baseMLabURL);
     httpClient.get('users?' + queryString + apikeyMLab,
       function(err, respuestaMLab, body){
         let response = body[0];
         console.log(body);
         //Actualizo campos del usuario
         let updatedUser = {
           "ID" : req.body.id,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };//Otra forma simplificada (para muchas propiedades)
         // var updatedUser = {};
         // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
         // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
         // PUT a mLab
         httpClient.put('users/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
           function(err, respuestaMLab, body){
             var response = {};
             if(err) {
                 response = {
                   "msg" : "Error actualizando usuario."
                 }
                 res.status(500);
             } else {
               if(body.length > 0) {
                 response = body;
               } else {
                 response = {
                   "msg" : "Usuario actualizado correctamente."
                 }
                 res.status(200);
               }
             }
             res.send(response);
           });
       });
 });

 //DELETE users
 app.delete(URL_BASE + "users/:id",
  function(req, res){
    var id = req.params.id;
    var queryStringID = 'q={"ID":' + id + '}&';

  //  var query2 = 'q={"id":${id}}&';

  //    var query3 = "q="+JSON.stringify({"id":id}) + '&';

    console.log(baseMLabURL + 'users?' + queryStringID + apikeyMLab);
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('users?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        httpClient.delete(baseMLabURL + "users/" + respuesta._id.$oid +'?'+ apikeyMLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });


//LOGIN
app.post(URL_BASE + "login2",
function loginUser(req, res){
   let email = req.body.email, pass=req.body.password;
   let queryString = `q={"email": "${email}","password":"${pass}"}&`;
   let limFilter = 'l=1&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log(queryString);
   httpClient.get('users?' +  queryString + limFilter + apikeyMLab,
     function(error, respuestaMLab, body){
       if(!error){
         if(body.length == 1){
           let login='{"$set":{"logged":true}}';
           console.log("ingresa a login")
           let stringQueryPut = `q={"ID":${body[0].ID}}&`;
           console.log(stringQueryPut);

           httpClient.put('users?'+ stringQueryPut + apikeyMLab, JSON.parse(login),
             function(errPut, resPut,bodyPut){
               res.send({'msg':'Login correcto','users':body[0].email,'userid':body[0].id});
           });
         }
         else {
           // console.log(respuestaMLab)
           response = {"msg" : "Ningún elemento 'user'."};
           res.status(404).send(response);
         }
         }
     });
   });

//LOGOUT

app.post(global.URL_BASE+'logout/:id',userlog.logout);


//peticion get de users con ID
app.get(global.URL_BASE+'users/:id/',users.getUsers);

//GET users con Query String
app.get(global.URL_BASE+'users',users.getUser2s);

//POST users
app.post(global.URL_BASE +'users',users.postUsers);

//PUT users
app.put(global.URL_BASE +'users',users.putUsers);
///////////////////////////////////////////////////////////
/*DELETE users*/
app.delete(global.URL_BASE+'users/:id',users.delUsers);

// LOGIN users
app.post(global.URL_BASE+'login',userlog.login);

// LOGOUT users
app.post(global.URL_BASE+'logout/:id',userlog.logout);

////////////////////////////////////////////////////////////////


//
//
//function logout(req,res){
//  console.log('*************LOGOUT*************** ');
//  let pos=req.params.id - 1;
//  if(user_file[pos] != undefined && user_file[pos].logged ){
//    delete user_file[pos].logged;
//    res.send({"mensaje":"logout correcto","id":user_file[pos].ID})
//  }
//  else {
//    rres.send({"mensaje":"logout incorrecto"});
//  }
//}

//var putBody = '{"$unset":{"logged":""}}'
//

//Servidor escuchando
app.listen(port, function () {
console.log('Example app listening on port 3000!');
})
