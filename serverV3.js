"use strict";

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var user_file=require('./user.json');
const global = require('./global');
var port=process.env.PORT || 3000;
app.use(bodyParser.json());
var users = require('./controllers/UserController');
var userlog = require('./controllers/UserLogcontrol');
app.use(bodyParser.json());
const URL_BASE= '/techu-peru/v2/'
var requestJSON = require('request-json');
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu42db/collections/';
const apikeyMLab = 'apiKey=NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF';

app.use(bodyParser.json());


// GET users consumiendo API REST de mLab
app.get(URL_BASE + 'users/:id',
 function(req, res) {
   var id = req.params.id;
   var queryStringID = 'q={"ID":' + id + '}&';
   console.log(queryStringID);
   console.log("GET /techu-peru/v2");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado.");
   var queryString = 'f={"_id":0}&';
   httpClient.get('users?' + queryString + queryStringID + apikeyMLab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
     });
});





// //peticion get
// app.get(global.URL_BASE +'users', function (req, res) {
// //res.send({"msg":"Operaciòn GET exitosa"});
// res.status(201).send(user_file);
// });

//peticion get de users con ID
app.get(global.URL_BASE+'users/:id/',users.getUsers);

//GET users con Query String
app.get(global.URL_BASE+'users',users.getUser2s);

//POST users
app.post(global.URL_BASE +'users',users.postUsers);

//PUT users
app.put(global.URL_BASE +'users',users.putUsers);
///////////////////////////////////////////////////////////
/*DELETE users*/
app.delete(global.URL_BASE+'users/:id',users.delUsers);

// LOGIN users
app.post(global.URL_BASE+'login',userlog.login);

// LOGOUT users
app.post(global.URL_BASE+'logout/:id',userlog.logout);

////////////////////////////////////////////////////////////////


//Servidor escuchando
app.listen(port, function () {
console.log('Example app listening on port 3000!');
})
