
var user_file=require('../user.json');
const global = require('../global');

//peticion get de users con ID
function getUsers(req,res){
  let pos = req.params.id - 1;
  console.log("GET con id=" + req.params.id);
  let tam = user_file.length;
  console.log(tam);
  let respuesta = (user_file[pos] == undefined) ?
  {"msg" : "usuario no encontrado"} : user_file[pos];
  res.send(respuesta);
}

//GET users con Query String

function getUser2s(req,res){
  console.log('GET de USERS');
//  console.log(req.query.id);
//  console.log(req.query.name);
  res.send(user_file);
}

//POST users

function postUsers(req,res){
  console.log('*************POST***************');
  //  console.log("Nuevo Usuario:" + req.body);
  //  console.log("Nuevo Usuario first_name:" + req.body.first_name);
  //  console.log("Nuevo Usuario email:" + req.body.email);

  let newID=user_file.length + 1;
  let newUser = {
    "id" : newID,
    "first_name": req.body.first_name,
    "last_name" : req.body.last_name,
    "email": req.body.email,
    "password" :req.body.password
  }

user_file.push(newUser);
console.log("nuevo usuario" + newUser);
res.send(newUser);
// res.send({"msg":"Post Exitoso"})
}

//PUT users

function putUsers(req,res){
  console.log('*************PUT***************');
  let pos=req.body.ID -1;

  let User = {
    "id" : req.body.ID,
    "first_name": req.body.first_name,
    "last_name" : req.body.last_name,
    "email": req.body.email,
    "password" :req.body.password
  }
user_file [pos] = User;
console.log("nombre Nuevo" + JSON.stringify(User));
res.send(User);
}

///////////////////////////////////////////////////////////

//DELETE users

function delUsers(req,res){
  console.log('*************DELETE*************** ');
  let pos=req.params.id;
  user_file.splice(pos-1,1);
  res.send({"msg":"usuario eliminado"});
}

// LOGIN users

function LoginUsers(req,res){
  console.log('*************LOGIN***************');
  console.log(req.body.email);
  console.log(req.body.password);
let tam=user_file.length;
let i= 0;
let encontrado = false;
while ((i<user_file.length) && !encontrado) {
  if (user_file[i].email==req.body.email && user_file[i].password==req.body.password) {
     encontrado = true;
     user_file[i].logged=true
  }
  i++;
  }
  if(encontrado){
  res.send({"encontrado":"si","id":i})
}
  else {
    res.send({"encontrado":"no"})
  }
}

////////////////////////////////////////////////////////////////
//LOGOUT users

function logoutUsers(req,res){
  console.log('*************LOGOUT*************** ');
  let pos=req.params.id - 1;
//  delete user_file[pos].logged;
//  logutID = true;
//  user_file.splice(pos-1,1);
  if(user_file[pos] != undefined && user_file[pos].logged ){
    delete user_file[pos].logged;
    res.send({"mensaje":"logout correcto","id":user_file[pos].ID})
  }
  else {
    rres.send({"mensaje":"logout incorrecto"});
  }
}

////////////////////////////////////////////////////////////////

module.exports = {
  getUsers,getUser2s,putUsers,postUsers,delUsers
};
